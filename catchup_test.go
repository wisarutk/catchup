package catchup

import (
	"fmt"
	"testing"
)

func TestCatchUp(t *testing.T) {
	out, err := CatchUp("10.18.14.168", "trfadm", "Iam0k&U?!", "/record_catchup/007_TrueSparkPlay_20200401_110459.mp4")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(out)
}
