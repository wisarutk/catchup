package catchup

import (
	"fmt"

	"github.com/helloyi/go-sshclient"
	"github.com/kpango/glg"
)

type InputParam struct {
	Hostname   string `json:"hostname"`
	Username   string `json:"username"`
	Password   string `json:"password"`
	AbPathFile string `json:"ab_path_file"`
}

func CatchUp(host, username, password, abPath string) (string, error) {
	client, err := sshclient.DialWithPasswd(host+":22", username, password)
	if err != nil {
		return "", err
	}
	defer client.Close()

	glg.Info("catchup file:", host, abPath)
	out, err := client.Cmd("sudo /bin/catchup " + abPath).SmartOutput()
	if err != nil {
		return "", err
	}

	fmt.Println("/bin/catchup "+abPath+":", string(out))

	trg, err := client.Cmd("ls " + string(out)).SmartOutput()
	if err != nil {
		return "", err
	}

	fmt.Println("ls "+string(out)+":", string(trg))

	return string(out), nil
}
